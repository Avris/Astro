install:
	composer install

start:
	symfony serve --daemon

stop:
	symfony server:stop

deploy:
	composer install --no-dev --optimize-autoloader
	bin/console cache:clear
