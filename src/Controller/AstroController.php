<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AstroController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @Route(
     *     "/{_locale}/{sign}/{date}",
     *     requirements={"_locale": "%languagesKeys%", "sign": "%signsKeys%", "date": "\d\d\d\d-\d\d-\d\d"},
     *     defaults={"_locale": "en", "sign": null, "date": null}
     * )
     */
    public function home(?string $sign = null, ?string $date = null)
    {
        return $this->render('base.html.twig', [
            'sign' => $sign,
            'date' => $date ? new \DateTimeImmutable($date) : null,
            'today' => new \DateTimeImmutable(),
        ]);
    }
}
