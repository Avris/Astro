<?php

namespace App;

use Avris\Suml\Symfony\SumlKernelTrait;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;
    use SumlKernelTrait;

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import('../config/{packages}/*.suml');
        $container->import('../config/{packages}/'.$this->environment.'/*.suml');

        if (is_file(\dirname(__DIR__).'/config/services.suml')) {
            $container->import('../config/{services}.suml');
            $container->import('../config/{services}_'.$this->environment.'.suml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/services.php')) {
            (require $path)($container->withPath($path), $this);
        }
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/'.$this->environment.'/*.suml');
        $routes->import('../config/{routes}/*.suml');

        if (is_file(\dirname(__DIR__).'/config/routes.suml')) {
            $routes->import('../config/{routes}.suml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/routes.php')) {
            (require $path)($routes->withPath($path), $this);
        }
    }
}
