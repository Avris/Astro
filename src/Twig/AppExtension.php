<?php

namespace App\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use WyriHaximus\HtmlCompress\Factory;
use WyriHaximus\HtmlCompress\HtmlCompressorInterface;

class AppExtension extends AbstractExtension
{
    private HtmlCompressorInterface $parser;

    public function __construct()
    {
        $this->parser = Factory::constructSmallest();
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('htmlcompress', function (Environment $twig, $html) {
                return $twig->isDebug() ? $html : $this->parser->compress($html);
            }, ['is_safe' => ['html'], 'needs_environment' => true])
        ];
    }
}
